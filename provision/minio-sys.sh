#!/usr/bin/env bash

curl -O https://raw.githubusercontent.com/minio/minio-service/master/linux-systemd/minio.service

mv minio.service /etc/systemd/system

systemctl daemon-reload

systemctl enable minio

systemctl start minio