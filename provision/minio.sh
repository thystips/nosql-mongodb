#!/usr/bin/env bash

useradd --system minio-user --shell /sbin/nologin

wget https://dl.minio.io/server/minio/release/linux-amd64/minio

chmod +x minio

mv minio /usr/local/bin

useradd -r minio-user -s /sbin/nologin

chown minio-user:minio-user /usr/local/bin/minio

mkdir /usr/local/share/minio

chown minio-user:minio-user /usr/local/share/minio

mkdir /etc/minio

chown minio-user:minio-user /etc/minio

cp /vagrant/provision/minio /etc/default/minio