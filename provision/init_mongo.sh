#!/usr/bin/env bash

cat /vagrant/provision/hosts >> /etc/hosts

apt-get install -y gnupg

wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" > /etc/apt/sources.list.d/mongodb-org-4.4.list

apt-get update

apt-get install -y mongodb-org mongosh

cp /vagrant/provision/mongod.conf /etc/mongod.conf

mkdir -p /etc/mongodb/keyFiles/
cp /vagrant/provision/mongo-key /etc/mongodb/keyFiles/mongo-key
chmod 400 /etc/mongodb/keyFiles/mongo-key
chown -R mongodb:mongodb /etc/mongodb

mkdir -p /data/db
chown -R mongodb:mongodb /data/db

systemctl daemon-reload
systemctl enable mongod
systemctl restart mongod

systemctl start mongod