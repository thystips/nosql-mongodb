const MongoClient = require('mongodb').MongoClient;
const Logger = require('mongodb').Logger;
const assert = require('assert');
const fetch = require('node-fetch');

// Connection URL
const url = 'mongodb://172.30.100.11:27017,mongodb://172.30.100.12:27017,mongodb://172.30.100.13:27017,mongodb://172.30.100.14:27017';
//
// // Database Name
const dbName = 'myproject';

async function main() {
    const client = await MongoClient.connect(url, {useUnifiedTopology: true, useNewUrlParser: true }); 
    console.log('Connected successfully to server');
    Logger.setLevel("debug");
    Logger.filter("class", ["Db", "ReplSet", "Pool"]);

    const db = client.db(dbName);
    await db.command({ isMaster: true });
    // await insertData(db)
    await findComment(db)
    await writePost(db)
    await lastPost(db)
    await client.close();
}

main().catch((err) => console.log(err))

const findComment = async function(db) {
    // Get the documents collection
    const posts = db.collection('posts');
    const comments = db.collection('comments');
    // Find some documents
    const docs = await posts.find({"userId": 1}).toArray()
    console.log('Nb Posts ' + docs.length);
    let nb_commentaires = 0
    const promises = docs.map(async (e) => {
        nb_commentaires += await comments.countDocuments({"postId": e.id})
    });
    await Promise.all(promises)
    console.log('Nb Commentaires total ' + nb_commentaires)
};

const writePost = async function(db) {
    const posts = db.collection('posts');
    const myPost = { "userId": 1, "title": 'test title', "body": 'test Body'}
    const res = await posts.insertOne(myPost)
    console.log(res.ops[0].title)
}

const lastPost = async function(db) {
    const posts = db.collection('posts')
    const docs = await posts.find({}).limit(1).sort({"_id": -1}).toArray()
    console.log(docs)
}

const insertData = async function(db) {
    const users = db.collection('users');
    const todos = db.collection('todos');
    const photos = db.collection('photos');
    const albums = db.collection('albums');
    const comments = db.collection('comments');
    const posts = db.collection('posts');

    await fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then(json => users.insertMany(json))
    await fetch('https://jsonplaceholder.typicode.com/todos')
        .then(res => res.json())
        .then(json => todos.insertMany(json))
    await fetch('https://jsonplaceholder.typicode.com/photos')
        .then(res => res.json())
        .then(json => photos.insertMany(json))
    await fetch('https://jsonplaceholder.typicode.com/albums')
        .then(res => res.json())
        .then(json => albums.insertMany(json))
    await fetch('https://jsonplaceholder.typicode.com/comments')
        .then(res => res.json())
        .then(json => comments.insertMany(json))
    await fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res => res.json())
        .then(json => posts.insertMany(json))
}